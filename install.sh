#!/usr/bin/env bash
set -ue
log () {
  echo >&3 "$@"
}
error () {
  log "$@"
  exit 1
}

exec 3>&2 2>/dev/null 1>/dev/null

[ "$(id -u)" = 0 ] || error "Script must be run as root."

ZERO="$(cd "$(dirname "${BASH_SOURCE[0]-$0}")" && pwd)"
log "* CHECK files..."
test -f "$ZERO/install.cfg"
for file in torrent-done.sh vpn-wait.sh; do
  test -f "$ZERO/src/$file" || error "Missing file src/$file"
done
for file in client.conf; do
  test -f "$ZERO/vpn/$file" || error "Missing file vpn/$file"
done

log "* CHECK config..."
source "$ZERO/install.cfg"
to_download_dir="${to_download_dir:-}"
test -n "$to_download_dir" || error "to_download_dir must be set"
to_incomplete_dir="${to_incomplete_dir:-}"
to_incomplete_enabled="$(test -n "$to_incomplete_dir" && echo true || echo false)"
to_rpc_username="${to_rpc_username:-transmission}"
to_rpc_password="${to_rpc_password:-}"
to_rpc_auth="$(test -n "$to_rpc_password" && echo true || echo false)"
to_rpc_whitelist="${to_rpc_whitelist:-}"
to_rpc_whitelist_enabled="$(test -n "$to_rpc_whitelist" && echo true || echo false)"
to_pushover_user="${to_pushover_user:-}"
to_pushover_token="${to_pushover_token:-}"
test -z "$to_pushover_user" || test -n "$to_pushover_token" || error "to_pushover_token must be set"
to_done_enabled="$(test -n "$to_pushover_user" && echo true || echo false)"

log "* APT update..."
apt update
log "* APT install..."
apt install -yq curl openvpn transmission-daemon net-tools sed

log "* SYSTEMCTL stop & disable..."
to_services="transmission-daemon openvpn openvpn@client"
systemctl stop $to_services
systemctl disable $to_services

to_country="$(curl -Ls ifconfig.co/country-iso)"
log "* CURRENT COUNTRY is : '$to_country'"
test -n "$to_country"

to_optdir="/opt/transmission-openvpn"
log "* MKDIR $to_optdir"
mkdir -p "$to_optdir"

log "* GENERATE $to_optdir/vpn-wait.sh"
sed -f- "$ZERO/src/vpn-wait.sh" >"$to_optdir/vpn-wait.sh" <<EOF
s%###COUNTRY###%$to_country%g
EOF
chmod 755 "$to_optdir/vpn-wait.sh"

log "* GENERATE $to_optdir/torrent-done.sh"
sed -f- "$ZERO/src/torrent-done.sh" >"$to_optdir/torrent-done.sh" <<EOF
s%###USER###%$to_pushover_user%g
s%###TOKEN###%$to_pushover_token%g
EOF
chmod 755 "$to_optdir/torrent-done.sh"

log "* GENERATE /etc/transmission-daemon/settings.json"
sed -f- "$ZERO/src/settings.json" >"/etc/transmission-daemon/settings.json" <<EOF
s%###DOWNLOAD###%$to_download_dir%g
s%###INCOMPLETE###%$to_incomplete_dir%g
s%###INCOMPLETEENABLED###%$to_incomplete_enabled%g
s%###AUTH###%$to_rpc_auth%g
s%###USERNAME###%$to_rpc_username%g
s%###PASSWORD###%$to_rpc_password%g
s%###WHITELIST###%$to_rpc_whitelist%g
s%###WHITELISTENABLED###%$to_rpc_whitelist_enabled%g
s%###DONEENABLED###%$to_done_enabled%g
s%###DONE###%$to_optdir/torrent-done.sh%g
EOF

log "* COPY vpn conf"
cp -f "$ZERO/vpn"/* "/etc/openvpn/."

log "* GENERATE systemd dependency"
sd="/lib/systemd/system/transmission-daemon.service"
cp -f "$sd" "${sd}.bkp"
sed -if- "$sd" <<EOF
/^ExecStartPre=/ { /vpn-wait\.sh/ d; }
/^ExecStart=/ i \ExecStartPre=$(which bash) $to_optdir/vpn-wait.sh | systemd-cat -t vpn
EOF

log "* SYSTEMCTL enable & start..."
systemctl enable $to_services
systemctl start $to_services
