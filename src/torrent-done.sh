#!/usr/bin/env bash
set -ue
po_user="###USER###"
po_token="###TOKEN###"
po_url=https://api.pushover.net/1/messages.json
po_title="Torrent Finished"
po_message="$TR_TORRENT_NAME"

generate_data () {
cat << EOF
{
  "user": "$po_user",
  "token": "$po_token",
  "title": "$po_title",
  "message": "$po_message"
}
EOF
}
curl  -H "Accept: application/json" -H "Content-Type: application/json" \
      -X POST \
      --data "$(generate_data)" \
      "$po_url"
set +ue
