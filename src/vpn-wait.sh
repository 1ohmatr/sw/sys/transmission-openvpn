#!/usr/bin/env bash
set -ue
test_country () {
  country="$(curl -Ls ifconfig.co/country-iso || echo)"
  echo >&2 "Country is: '${country:-none}'"
  test -n "$country"
  ! grep -q '###COUNTRY###' <<< "$country"
}

for i in `seq 1 10`; do
  ! test_country || exit 0
  sleep 5
done
exit 1
